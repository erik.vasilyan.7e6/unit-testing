import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 19/09/2022
* TITLE: Pupitres
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Type the number of students in first class: ")
    val firstClassStudents = scanner.nextInt()
    print("Type the number of students in second class: ")
    val secondClassStudents = scanner.nextInt()
    print("Type the number of students in third class: ")
    val thirdClassStudents = scanner.nextInt()

    val result = calculateNOfTables(firstClassStudents, secondClassStudents, thirdClassStudents)

    println("Result: $result")
}

fun calculateNOfTables(firstClassStudents: Int, secondClassStudents: Int, thirdClassStudents: Int): Int {
    val rest = (firstClassStudents + secondClassStudents + thirdClassStudents) % 2
    return if(firstClassStudents < 0 || secondClassStudents < 0 || thirdClassStudents < 0) 0
    else ((firstClassStudents + secondClassStudents + thirdClassStudents) / 2) + rest
}
import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 21/09/2022
* TITLE: Afegeix un segon
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Introdueix els segons: ")
    val segons = scanner.nextInt()

    println(sumOneSecond(segons))
}

fun sumOneSecond(segons: Int): Int {
    return (segons+1) % 60
}
import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 21/09/2022
* TITLE: Quina temperatura fa?
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Type the temperature: ")
    val temperature = scanner.nextDouble()
    print("Type the increase of the temperature: ")
    val increase = scanner.nextDouble()

    val actualTemperature = calculateActualTemperature(temperature, increase)

    println("La temperatura actual es: $actualTemperature\u2103")
}

fun calculateActualTemperature(temperature: Double, increase: Double): Double {
    return temperature + increase
}
import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 21/09/2022
* TITLE: Calculadora de volum d’aire
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Type the length: ")
    val length = scanner.nextDouble()
    print("Type the width: ")
    val width= scanner.nextDouble()
    print("Type the height: ")
    val height = scanner.nextDouble()

    val volume = calculateVolume(length, width, height)

    println("Volume: $volume")
}

fun calculateVolume(length: Double, width: Double, height: Double): Double {
    return if (length <= 0 || width <= 0 || height <= 0) 0.0
    else length * width * height
}
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

@Suppress("ClassName")
internal class _1_3KtTest {

    @Test
    fun sumOf5And9Is14() {
        val expected = 14
        assertEquals(expected, sumOfNumbers(5, 9))
    }

    @Test
    fun sumOfZeroAnd10Is10() {
        val expected = 10
        assertEquals(expected, sumOfNumbers(0, 10))
    }

    @Test
    fun sumOfNegative6AndPositive18Is12() {
        val expected = 12
        assertEquals(expected, sumOfNumbers(-6, 18))
    }
}
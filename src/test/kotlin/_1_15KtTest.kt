import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

@Suppress("ClassName")
internal class _1_15KtTest {

    @Test
    fun sumOfOneSecondTo55SecondsIs56() {
        val expected = 56
        assertEquals(expected, sumOneSecond(55))
    }

    @Test
    fun sumOfOneSecondTo59SecondsIs0() {
        val expected = 0
        assertEquals(expected, sumOneSecond(59))
    }

    @Test
    fun sumOfOneSecondTo0SecondsIs1() {
        val expected = 1
        assertEquals(expected, sumOneSecond(0))
    }
}
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

@Suppress("ClassName")
internal class _1_8KtTest {

    @Test
    fun doubleOf_3_14_Is_6_28() {
        val expected = 6.28
        assertEquals(expected, doubleDecimal(3.14))
    }

    @Test
    fun doubleOf_Negative_10_3_Is_6_28() {
        val expected = -20.6
        assertEquals(expected, doubleDecimal(-10.3))
    }

    @Test
    fun doubleOf_0_Is_0() {
        val expected = 0.0
        assertEquals(expected, doubleDecimal(0.0))
    }
}
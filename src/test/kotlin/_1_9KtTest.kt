import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

@Suppress("ClassName")
internal class _1_9KtTest {

    @Test
    fun ifOriginalPriseIs200AndDiscountPriseIs130theTotalDiscountIs35() {
        val expected = 35.0
        assertEquals(expected, calculateDiscount(200.0, 130.0))
    }

    @Test
    fun ifOriginalPriseIs200AndDiscountPriseIs0theTotalDiscountIs100() {
        val expected = 100.0
        assertEquals(expected, calculateDiscount(200.0, 0.0))
    }

    @Test
    fun ifOriginalPriseIsNegative200AndDiscountPriseIs80theTotalDiscountIs0() {
        val expected = 0.0
        assertEquals(expected, calculateDiscount(-200.0, 80.0))
    }
}
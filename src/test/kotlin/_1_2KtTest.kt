import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

@Suppress("ClassName")
internal class _1_2KtTest {

    @Test
    fun doubleOf2Is4() {
        val expected = 4
        assertEquals(expected, doubleNumber(2))
    }

    @Test
    fun doubleOfNegative2IsNegative4() {
        val expected = -4
        assertEquals(expected, doubleNumber(-2))
    }

    @Test
    fun doubleOfZeroIsZero() {
        val expected = 0
        assertEquals(expected, doubleNumber(0))
    }
}
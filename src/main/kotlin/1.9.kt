import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 19/09/2022
* TITLE: Calcula el descompte
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Type the original prise: ")
    val originalPrise = scanner.nextDouble()
    print("Type the current prise: ")
    val currentPrise = scanner.nextDouble()

    val discount = calculateDiscount(originalPrise, currentPrise)

    print("Discount: $discount")
}

fun calculateDiscount(originalPrise: Double, currentPrise: Double): Double {
    val totalDiscount = (currentPrise * 100) / originalPrise
    return if (originalPrise <= 0 || currentPrise < 0) 0.0
    else 100 - totalDiscount
}
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

@Suppress("ClassName")
internal class _1_11KtTest {

    @Test
    fun ifLengthIs10WidthIs20HeightIs30VolumeWillBe6000() {
        val expected = 6000.0
        assertEquals(expected, calculateVolume(10.0, 20.0, 30.0))
    }

    @Test
    fun ifLengthIs0WidthIs20HeightIs30VolumeWillBe0() {
        val expected = 0.0
        assertEquals(expected, calculateVolume(0.0, 20.0, 30.0))
    }

    @Test
    fun ifLengthIsNegative22WidthIs20HeightIs30VolumeWillBe0() {
        val expected = 0.0
        assertEquals(expected, calculateVolume(-22.0, 20.0, 30.0))
    }
}
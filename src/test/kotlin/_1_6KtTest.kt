import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

@Suppress("ClassName")
internal class _1_6KtTest {

    @Test
    fun ifStudentsAre_30_40_50_TablesAre() {
        val expected = 60
        assertEquals(expected, calculateNOfTables(30,40,50))
    }

    @Test
    fun ifStudentsAre_0_30_10_TablesAre() {
        val expected = 20
        assertEquals(expected, calculateNOfTables(0,30,10))
    }

    @Test
    fun ifStudentsAre_Negative10_30_20_TablesAre() {
        val expected = 0
        assertEquals(expected, calculateNOfTables(-10,30,20))
    }
}
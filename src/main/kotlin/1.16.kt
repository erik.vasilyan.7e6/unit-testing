import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 21/09/2022
* TITLE: Transforma l’enter
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Introdueix un nombre enter: ")
    var number1 = scanner.nextInt()

    println(transformIntToDouble(number1))
}

fun transformIntToDouble(number1: Int): Double {
    return number1.toDouble()
}
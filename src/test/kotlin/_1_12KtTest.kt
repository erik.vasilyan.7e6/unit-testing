import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

@Suppress("ClassName")
internal class _1_12KtTest {

    @Test
    fun ifTemperatureInCelsiusIs_20_5_InFahrenheitItWillBe_68_9() {
        val expected = 68.9
        assertEquals(expected, calculateFahrenheit(20.5))
    }

    @Test
    fun ifTemperatureInCelsiusIs0InFahrenheitItWillBe32() {
        val expected = 32.0
        assertEquals(expected, calculateFahrenheit(0.0))
    }

    @Test
    fun ifTemperatureInCelsiusIsNegative10InFahrenheitItWillBe14() {
        val expected = 14.0
        assertEquals(expected, calculateFahrenheit(-10.0))
    }
}
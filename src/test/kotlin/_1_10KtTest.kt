import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

@Suppress("ClassName")
internal class _1_10KtTest {

    @Test
    fun ifDiameterOfPizzaIs30AreaWillBe_706_86() {
        val expected = 706.50
        assertEquals(expected, calculateAreaOfRound(30.0))
    }

    @Test
    fun ifDiameterOfPizzaIs0AreaWillBe0() {
        val expected = 0.0
        assertEquals(expected, calculateAreaOfRound(0.0))
    }

    @Test
    fun ifDiameterOfPizzaIsNegative20AreaWillBe0() {
        val expected = 0.0
        assertEquals(expected, calculateAreaOfRound(-20.0))
    }
}
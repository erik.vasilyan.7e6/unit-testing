import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 21/09/2022
* TITLE: De Celsius a Fahrenheit
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Type the temperature in Celsius: ")
    val celsiusTemperature = scanner.nextDouble()

    val fahrenheitTemperature = calculateFahrenheit(celsiusTemperature)

    println("Temperature in Fahrenheit: $fahrenheitTemperature")
}

fun calculateFahrenheit(celsiusTemperature: Double): Double {
    return celsiusTemperature * 1.8 + 32
}
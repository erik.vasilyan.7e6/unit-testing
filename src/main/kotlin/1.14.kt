import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 21/09/2022
* TITLE: Divisor de compte
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Introdueix el número de començals: ")
    val comencals = scanner.nextInt()
    print("Introdueix el preu d'un sopar: ")
    val preu = scanner.nextDouble()

    val cost = checkDivisor(preu, comencals)

    println("Cost: $cost\u20AC")
}

fun checkDivisor(preu: Double, comencals: Int): Double {
    return if (preu < 0 || comencals < 1) 0.0
    else preu / comencals
}
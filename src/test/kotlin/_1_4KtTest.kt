import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

@Suppress("ClassName")
internal class _1_4KtTest {

    @Test
    fun ifWidthIs10AndHeightIs20AreaIs200() {
        val expected = 200
        assertEquals(expected, calculateArea(10, 20))
    }

    @Test
    fun ifWidthIs0AndHeightIs14AreaIs0() {
        val expected = 0
        assertEquals(expected, calculateArea(0, 14))
    }

    @Test
    fun ifWidthIsNegative10AndHeightIs5AreaIs0() {
        val expected = 0
        assertEquals(expected, calculateArea(-10, 5))
    }
}
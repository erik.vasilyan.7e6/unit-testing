import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

@Suppress("ClassName")
internal class _1_16KtTest {

    @Test
    fun doubleOfIntNumber4Is_4_0() {
        val expected = 4.0
        assertEquals(expected, transformIntToDouble(4))
    }

    @Test
    fun doubleOfIntNumber0Is_0_0() {
        val expected = 0.0
        assertEquals(expected, transformIntToDouble(0))
    }

    @Test
    fun doubleOfIntNegativeNumber10IsNegative_10_0() {
        val expected = -10.0
        assertEquals(expected, transformIntToDouble(-10))
    }
}
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

@Suppress("ClassName")
internal class _1_14KtTest {

    @Test
    fun ifPriceIs125AndNOfPeopleIs3EachPersonNeedToPay40() {
        val expected = 40.0
        assertEquals(expected, checkDivisor(120.0, 3))
    }

    @Test
    fun ifPriceIs0AndNOfPeopleIs5EachPersonNeedToPay0() {
        val expected = 0.0
        assertEquals(expected, checkDivisor(0.0, 5))
    }

    @Test
    fun ifPriceIsNegative50AndNOfPeopleIs5EachPersonNeedToPay0() {
        val expected = 0.0
        assertEquals(expected, checkDivisor(-50.0, 5))
    }
}
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

@Suppress("ClassName")
internal class _1_5KtTest {

    @Test
    fun crazyOperationOf_1_2_3_4_is9() {
        val expected = 9
        assertEquals(expected, crazyOperation(1, 2, 3, 4))
    }

    @Test
    fun crazyOperationOf_Negative3_5_0_9_is0() {
        val expected = 0
        assertEquals(expected, crazyOperation(-3, 5, 0, 9))
    }

    @Test
    fun crazyOperationOf_Negative3_Negative7_4_Negative6_is0() {
        val expected = -40
        assertEquals(expected, crazyOperation(-3, -7, 4, -6))
    }
}
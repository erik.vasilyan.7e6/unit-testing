import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 19/09/2022
* TITLE: Suma de dos nombres enters
*/

fun main() {
    val scanner = Scanner(System.`in`)
    print("Type the first value: ")
    val userInputFirstValue = scanner.nextInt()
    print("Type the second value: ")
    val userInputSecondValue = scanner.nextInt()

    val result = sumOfNumbers(userInputFirstValue, userInputSecondValue)

    println("Result: $result")
}

fun sumOfNumbers(num1:Int, num2:Int): Int {
    return num1 + num2
}
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

@Suppress("ClassName")
internal class _1_7KtTest {

    @Test
    fun nextNumberOf3Is4() {
        val expected = 4
        assertEquals(expected, nextNumber(3))
    }

    @Test
    fun nextNumberOf0Is1() {
        val expected = 1
        assertEquals(expected, nextNumber(0))
    }

    @Test
    fun nextNumberOfNegative10IsNegative11() {
        val expected = -11
        assertEquals(expected, nextNumber(-10))
    }
}
import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 19/09/2022
* TITLE: Número següent
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Type a number: ")
    val firstNumber = scanner.nextInt()

    val result = nextNumber(firstNumber)
    print("Després ve el: $result")
}

fun nextNumber(firstNumber: Int): Int {
    return if (firstNumber >= 0) firstNumber + 1
    else firstNumber - 1
}
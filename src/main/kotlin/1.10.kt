import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 19/09/2022
* TITLE: Quina és la mida de la meva pizza?
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Type the diameter of a round: ")
    val diameter = scanner.nextDouble() 

    val area = calculateAreaOfRound(diameter)

    print("Area: ${String.format("%.2f", area)}")
}

fun calculateAreaOfRound(diameter: Double): Double {
    return if (diameter <= 0) 0.0
    else 3.14 * ((diameter / 2) * (diameter / 2))
}
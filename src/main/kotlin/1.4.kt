import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 19/09/2022
* TITLE: Calcula l’àrea
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Type the width: ")
    val width = scanner.nextInt()
    print("Type the height: ")
    val height = scanner.nextInt()

    val area = calculateArea(width, height)
    print("Area: $area")
}

fun calculateArea (width:Int, height:Int): Int {
    return if (width * height > 0) width*height
    else 0
}
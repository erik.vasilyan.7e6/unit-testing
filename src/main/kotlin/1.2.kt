import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 19/09/2022
* TITLE: Dobla l'enter
*/

fun main() {
    val scanner = Scanner(System.`in`)
    print("Type a number: ")
    val userInputValue = scanner.nextInt()

    val result = doubleNumber(userInputValue)
    println("Result: $result")
}

fun doubleNumber(num: Int): Int {
    return num * 2
}
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

@Suppress("ClassName")
internal class _1_13KtTest {

    @Test
    fun ifActualTemperatureIs_10_5_AndItIncreases_4_3_TheActualTemperatureWillBe_14_8() {
        val expected = 14.8
        assertEquals(expected, calculateActualTemperature(10.5, 4.3))
    }

    @Test
    fun ifActualTemperatureIs0_AndItIncreases_7_1_TheActualTemperatureWillBe_7_1() {
        val expected = 7.1
        assertEquals(expected, calculateActualTemperature(0.0, 7.1))
    }

    @Test
    fun ifActualTemperatureIs_Negative_3_5_AndItIncreases_5_5_TheActualTemperatureWillBe2() {
        val expected = 2.0
        assertEquals(expected, calculateActualTemperature(-3.5, 5.5))
    }
}
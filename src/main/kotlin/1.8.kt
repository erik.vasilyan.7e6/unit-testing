import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 19/09/2022
* TITLE: Dobla el decimal
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Type a number: ")
    val firstNumber = scanner.nextDouble()

    val result = doubleDecimal(firstNumber)

    print("Result: $result")
}

fun doubleDecimal(firstNumber: Double): Double {
    return firstNumber * 2
}